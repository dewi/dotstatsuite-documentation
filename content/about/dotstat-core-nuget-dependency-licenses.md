---
title: "Licenses of NuGet dependencies of .Stat Core module"
subtitle: 
comments: false
weight: 18
keywords: [
  'dotstatsuite-core-common', '#dotstatsuite-core-common',
  'dotstatsuite-core-data-access', '#dotstatsuite-core-data-access',
  'dotstatsuite-core-transfer', '#dotstatsuite-core-transfer',
  'dotstatsuite-core-auth-management', '#dotstatsuite-core-auth-management',
]
---

#### Table of Content
- [dotstatsuite-core-common](#dotstatsuite-core-common)
- [dotstatsuite-core-data-access](#dotstatsuite-core-data-access)
- [dotstatsuite-core-transfer](#dotstatsuite-core-transfer)
- [dotstatsuite-core-auth-management](#dotstatsuite-core-auth-management)

---

### dotstatsuite-core-common

| NuGet package | Author | License |
|---|---|---|
| [Castle.Core](https://www.nuget.org/packages/Castle.Core/) | Castle Project Contributors | [Apache License Version 2.0.](https://github.com/castleproject/Core/blob/master/LICENSE) | 
| [log4net](https://www.nuget.org/packages/log4net/) | The Apache Software Foundation | [Apache License Version 2.0.](http://logging.apache.org/log4net/license.html) | 
| [MicroKnights.Log4NetAdoNetAppender](https://www.nuget.org/packages/MicroKnights.Log4NetAdoNetAppender/) | Frank Løvendahl Nielsen | [Apache License Version 2.0.](https://github.com/microknights/Log4NetAdoNetAppender/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.Http.Abstractions](https://www.nuget.org/packages/Microsoft.AspNetCore.Http.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/HttpAbstractions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Moq](https://www.nuget.org/packages/Moq/) | Daniel Cazzulino, kzu | [BSD 3-Clause License](https://github.com/moq/moq4/blob/master/License.txt) | 
| [Nerdbank.GitVersioning](https://www.nuget.org/packages/Nerdbank.GitVersioning/) | Andrew Arnott | [MIT](https://github.com/AArnott/Nerdbank.GitVersioning/blob/master/LICENSE.txt) | 
| [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/) | James Newton-King | [MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md) | 
| [NUnit](https://www.nuget.org/packages/NUnit/) | Charlie Poole, Rob Prouse | [MIT](https://github.com/nunit/nunit/blob/master/LICENSE.txt) | 
| [NUnit3TestAdapter](https://www.nuget.org/packages/NUnit3TestAdapter/) | Charlie Poole, Terje Sandstrom | [MIT](https://github.com/nunit/nunit3-vs-adapter/blob/master/LICENSE.txt) | 

### dotstatsuite-core-data-access

| NuGet package | Author | License |
|---|---|---|
| [Castle.Core](https://www.nuget.org/packages/Castle.Core/) | Castle Project Contributors | [Apache License Version 2.0.](https://github.com/castleproject/Core/blob/master/LICENSE) | 
| [CsvHelper](https://www.nuget.org/packages/CsvHelper/) | Josh Close | [Microsoft Public License (MS-PL) & Apache License Version 2.0.](https://github.com/JoshClose/CsvHelper/blob/master/LICENSE.txt) |
| [Dapper](https://www.nuget.org/packages/Dapper/) | Sam Saffron, Marc Gravell, Nick Craver | [Apache License Version 2.0.](https://github.com/StackExchange/Dapper/blob/master/License.txt) | 
| [DotStat.Common](https://www.nuget.org/packages/DotStat.Common/) | OECD | [MIT](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/blob/develop/LICENSE) | 
| [DryIoc](https://www.nuget.org/packages/DryIoc/) | Maksim Volkau | [MIT](https://github.com/dadhi/DryIoc/blob/master/LICENSE.txt) | 
| [Estat.SdmxSource.Extension](https://www.nuget.org/packages/Estat.SdmxSource.Extension/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/estat_sdmxsource_extension.net/browse/EUPL_v1.1.txt) | 
| [Estat.SdmxSource.SdmxAPI](https://www.nuget.org/packages/Estat.SdmxSource.SdmxAPI/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxDataParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxDataParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxEdiParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxEdiParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxMlConstants](https://www.nuget.org/packages/Estat.SdmxSource.SdmxMlConstants/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxObjects](https://www.nuget.org/packages/Estat.SdmxSource.SdmxObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxParseBase](https://www.nuget.org/packages/Estat.SdmxSource.SdmxParseBase/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxSourceUtil](https://www.nuget.org/packages/Estat.SdmxSource.SdmxSourceUtil/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxStructureParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxStructureParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxStructureRetrieval](https://www.nuget.org/packages/Estat.SdmxSource.SdmxStructureRetrieval/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.StructureMutableParser](https://www.nuget.org/packages/Estat.SdmxSource.StructureMutableParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.Sri.Mapping.Api](https://www.nuget.org/packages/Estat.Sri.Mapping.Api/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Sdmx.MappingStore.Retrieve](https://www.nuget.org/packages/Estat.Sri.Sdmx.MappingStore.Retrieve/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Sdmx.MappingStore.Store](https://www.nuget.org/packages/Estat.Sri.Sdmx.MappingStore.Store/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Utils](https://www.nuget.org/packages/Estat.Sri.Utils/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [FluentAssertions](https://www.nuget.org/packages/FluentAssertions/) | Dennis Doomen, Jonas Nyrup | [Apache License Version 2.0.](https://github.com/fluentassertions/fluentassertions/blob/master/LICENSE) | 
| [log4net](https://www.nuget.org/packages/log4net/) | The Apache Software Foundation | [Apache License Version 2.0.](http://logging.apache.org/log4net/license.html) | 
| [MicroKnights.Log4NetAdoNetAppender](https://www.nuget.org/packages/MicroKnights.Log4NetAdoNetAppender/) | Frank Løvendahl Nielsen | [Apache License Version 2.0.](https://github.com/microknights/Log4NetAdoNetAppender/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.Http.Abstractions](https://www.nuget.org/packages/Microsoft.AspNetCore.Http.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/HttpAbstractions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.Configuration.Binder](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Binder/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.NET.Test.Sdk](https://www.nuget.org/packages/Microsoft.NET.Test.Sdk/) | Microsoft | [MIT](https://github.com/microsoft/vstest/blob/master/LICENSE) | 
| [Moq](https://www.nuget.org/packages/Moq/) | Daniel Cazzulino, kzu | [BSD 3-Clause License](https://github.com/moq/moq4/blob/master/License.txt) | 
| [Nerdbank.GitVersioning](https://www.nuget.org/packages/Nerdbank.GitVersioning/) | Andrew Arnott | [MIT](https://github.com/AArnott/Nerdbank.GitVersioning/blob/master/LICENSE.txt) | 
| [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/) | James Newton-King | [MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md) | 
| [NUnit](https://www.nuget.org/packages/NUnit/) | Charlie Poole, Rob Prouse | [MIT](https://github.com/nunit/nunit/blob/master/LICENSE.txt) | 
| [NUnit3TestAdapter](https://www.nuget.org/packages/NUnit3TestAdapter/) | Charlie Poole, Terje Sandstrom | [MIT](https://github.com/nunit/nunit3-vs-adapter/blob/master/LICENSE.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V10](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V10/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V20](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V20/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V21](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V21/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [XObjects](https://www.nuget.org/packages/XObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) |


### dotstatsuite-core-transfer

| NuGet package | Author | License |
|---|---|---|
| [CsvHelper](https://www.nuget.org/packages/CsvHelper/) | Josh Close | [Microsoft Public License (MS-PL) & Apache License Version 2.0.](https://github.com/JoshClose/CsvHelper/blob/master/LICENSE.txt) |
| [Dapper](https://www.nuget.org/packages/Dapper/) | Sam Saffron, Marc Gravell, Nick Craver | [Apache License Version 2.0.](https://github.com/StackExchange/Dapper/blob/master/License.txt) | 
| [DotStat.Common](https://www.nuget.org/packages/DotStat.Common/) | OECD | [MIT](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/blob/develop/LICENSE) | 
| [DotStat.DataAccess](https://www.nuget.org/packages/DotStat.DataAccess.NuGet/) | OECD | [MIT](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/blob/develop/LICENSE) | 
| [DryIoc](https://www.nuget.org/packages/DryIoc/) | Maksim Volkau | [MIT](https://github.com/dadhi/DryIoc/blob/master/LICENSE.txt) | 
| [DryIoc.Microsoft.DependencyInjection](https://www.nuget.org/packages/DryIoc.Microsoft.DependencyInjection/) | Maksim Volkau | [MIT](https://github.com/dadhi/DryIoc/blob/master/LICENSE.txt) | 
| [EPPlus](https://www.nuget.org/packages/EPPlus/) | Jan Källman | [LGPL-3.0-or-later](https://github.com/JanKallman/EPPlus#license) | 
| [Estat.SdmxSource.Extension](https://www.nuget.org/packages/Estat.SdmxSource.Extension/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/estat_sdmxsource_extension.net/browse/EUPL_v1.1.txt) | 
| [Estat.SdmxSource.SdmxAPI](https://www.nuget.org/packages/Estat.SdmxSource.SdmxAPI/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxDataParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxDataParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxEdiParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxEdiParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxMlConstants](https://www.nuget.org/packages/Estat.SdmxSource.SdmxMlConstants/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxObjects](https://www.nuget.org/packages/Estat.SdmxSource.SdmxObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxParseBase](https://www.nuget.org/packages/Estat.SdmxSource.SdmxParseBase/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxSourceUtil](https://www.nuget.org/packages/Estat.SdmxSource.SdmxSourceUtil/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxStructureParser](https://www.nuget.org/packages/Estat.SdmxSource.SdmxStructureParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxStructureRetrieval](https://www.nuget.org/packages/Estat.SdmxSource.SdmxStructureRetrieval/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.StructureMutableParser](https://www.nuget.org/packages/Estat.SdmxSource.StructureMutableParser/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.Sri.Mapping.Api](https://www.nuget.org/packages/Estat.Sri.Mapping.Api/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Mapping.MappingStore](https://www.nuget.org/packages/Estat.Sri.Mapping.MappingStore/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Sdmx.MappingStore.Retrieve](https://www.nuget.org/packages/Estat.Sri.Sdmx.MappingStore.Retrieve/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Sdmx.MappingStore.Store](https://www.nuget.org/packages/Estat.Sri.Sdmx.MappingStore.Store/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Utils](https://www.nuget.org/packages/Estat.Sri.Utils/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [log4net](https://www.nuget.org/packages/log4net/) | The Apache Software Foundation | [Apache License Version 2.0.](http://logging.apache.org/log4net/license.html) | 
| [MicroKnights.Log4NetAdoNetAppender](https://www.nuget.org/packages/MicroKnights.Log4NetAdoNetAppender/) | Frank Løvendahl Nielsen | [Apache License Version 2.0.](https://github.com/microknights/Log4NetAdoNetAppender/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.App](https://www.nuget.org/packages/Microsoft.AspNetCore.App/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Http.Abstractions](https://www.nuget.org/packages/Microsoft.AspNetCore.Http.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/HttpAbstractions/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.ApiExplorer](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.ApiExplorer/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Mvc/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Core](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Core/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.DataAnnotations](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.DataAnnotations/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Mvc/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Formatters.Json](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Formatters.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Versioning](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Versioning/) | Microsoft | [MIT](https://github.com/microsoft/aspnet-api-versioning/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.Routing](https://www.nuget.org/packages/Microsoft.AspNetCore.Routing/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.StaticFiles](https://www.nuget.org/packages/Microsoft.AspNetCore.StaticFiles/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/StaticFiles/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.DependencyInjection.Abstractions](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.FileProviders.Embedded](https://www.nuget.org/packages/Microsoft.Extensions.FileProviders.Embedded/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/) | James Newton-King | [MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V10](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V10/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V20](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V20/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V21](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V21/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Swashbuckle.AspNetCore](https://www.nuget.org/packages/Swashbuckle.AspNetCore/) | Swashbuckle.AspNetCore | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.Swagger](https://www.nuget.org/packages/Swashbuckle.AspNetCore.Swagger/) | Swashbuckle.AspNetCore.Swagger | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.SwaggerGen](https://www.nuget.org/packages/Swashbuckle.AspNetCore.SwaggerGen/) | Swashbuckle.AspNetCore.SwaggerGen | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.SwaggerUI](https://www.nuget.org/packages/Swashbuckle.AspNetCore.SwaggerUI/) | Swashbuckle.AspNetCore.SwaggerUI | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [XObjects](https://www.nuget.org/packages/XObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) |

### dotstatsuite-core-auth-management

| NuGet package | Author | License |
|---|---|---|
| [Dapper](https://www.nuget.org/packages/Dapper/) | Sam Saffron, Marc Gravell, Nick Craver | [Apache License Version 2.0.](https://github.com/StackExchange/Dapper/blob/master/License.txt) | 
| [DotStat.Common](https://www.nuget.org/packages/DotStat.Common/) | OECD | [MIT](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/blob/develop/LICENSE) | 
| [DotStat.DataAccess](https://www.nuget.org/packages/DotStat.DataAccess.NuGet/) | OECD | [MIT](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/blob/develop/LICENSE) | 
| [DryIoc](https://www.nuget.org/packages/DryIoc/) | Maksim Volkau | [MIT](https://github.com/dadhi/DryIoc/blob/master/LICENSE.txt) | 
| [DryIoc.Microsoft.DependencyInjection](https://www.nuget.org/packages/DryIoc.Microsoft.DependencyInjection/) | Maksim Volkau | [MIT](https://github.com/dadhi/DryIoc/blob/master/LICENSE.txt) | 
| [Estat.SdmxSource.Extension](https://www.nuget.org/packages/Estat.SdmxSource.Extension/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/estat_sdmxsource_extension.net/browse/EUPL_v1.1.txt) | 
| [Estat.SdmxSource.SdmxAPI](https://www.nuget.org/packages/Estat.SdmxSource.SdmxAPI/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxObjects](https://www.nuget.org/packages/Estat.SdmxSource.SdmxObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxSourceUtil](https://www.nuget.org/packages/Estat.SdmxSource.SdmxSourceUtil/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.SdmxSource.SdmxStructureRetrieval](https://www.nuget.org/packages/Estat.SdmxSource.SdmxStructureRetrieval/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Estat.Sri.Mapping.Api](https://www.nuget.org/packages/Estat.Sri.Mapping.Api/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [Estat.Sri.Sdmx.MappingStore.Retrieve](https://www.nuget.org/packages/Estat.Sri.Sdmx.MappingStore.Retrieve/) | European Commission (Eurostat) | [European Union Public Licence v1.1](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/maapi.net/browse/licence.txt) | 
| [FluentAssertions](https://www.nuget.org/packages/FluentAssertions/) | Dennis Doomen, Jonas Nyrup | [Apache License Version 2.0.](https://github.com/fluentassertions/fluentassertions/blob/master/LICENSE) | 
| [FluentValidation](https://www.nuget.org/packages/FluentValidation/) | Jeremy Skinner | [Apache License Version 2.0.](https://github.com/JeremySkinner/FluentValidation/blob/master/License.txt) | 
| [FluentValidation.AspNetCore](https://www.nuget.org/packages/FluentValidation.AspNetCore/) | Jeremy Skinner | [Apache License Version 2.0.](https://github.com/JeremySkinner/FluentValidation/blob/master/License.txt) | 
| [FluentValidation.DependencyInjectionExtensions](https://www.nuget.org/packages/FluentValidation.DependencyInjectionExtensions/) | Jeremy Skinner | [Apache License Version 2.0.](https://github.com/JeremySkinner/FluentValidation/blob/master/License.txt) | 
| [MicroKnights.Log4NetAdoNetAppender](https://www.nuget.org/packages/MicroKnights.Log4NetAdoNetAppender/) | Frank Løvendahl Nielsen | [Apache License Version 2.0.](https://github.com/microknights/Log4NetAdoNetAppender/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.App](https://www.nuget.org/packages/Microsoft.AspNetCore.App/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Http.Abstractions](https://www.nuget.org/packages/Microsoft.AspNetCore.Http.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/HttpAbstractions/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Mvc/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.ApiExplorer](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.ApiExplorer/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Mvc/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Core](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Core/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.DataAnnotations](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.DataAnnotations/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Mvc/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Formatters.Json](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Formatters.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.Mvc.Versioning](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Versioning/) | Microsoft | [MIT](https://github.com/microsoft/aspnet-api-versioning/blob/master/LICENSE) | 
| [Microsoft.AspNetCore.Routing](https://www.nuget.org/packages/Microsoft.AspNetCore.Routing/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/AspNetCore/blob/master/LICENSE.txt) | 
| [Microsoft.AspNetCore.StaticFiles](https://www.nuget.org/packages/Microsoft.AspNetCore.StaticFiles/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/StaticFiles/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.DependencyInjection.Abstractions](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection.Abstractions/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Microsoft.Extensions.FileProviders.Embedded](https://www.nuget.org/packages/Microsoft.Extensions.FileProviders.Embedded/) | Microsoft | [Apache License Version 2.0.](https://github.com/aspnet/Extensions/blob/master/LICENSE.txt) | 
| [Nerdbank.GitVersioning](https://www.nuget.org/packages/Nerdbank.GitVersioning/) | Andrew Arnott | [MIT](https://github.com/AArnott/Nerdbank.GitVersioning/blob/master/LICENSE.txt) | 
| [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/) | James Newton-King | [MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V10](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V10/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V20](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V20/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Org.Sdmx.Resources.SdmxMl.Schemas.V21](https://www.nuget.org/packages/Org.Sdmx.Resources.SdmxMl.Schemas.V21/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) | 
| [Swashbuckle.AspNetCore](https://www.nuget.org/packages/Swashbuckle.AspNetCore/) | Swashbuckle.AspNetCore | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.Swagger](https://www.nuget.org/packages/Swashbuckle.AspNetCore.Swagger/) | Swashbuckle.AspNetCore.Swagger | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.SwaggerGen](https://www.nuget.org/packages/Swashbuckle.AspNetCore.SwaggerGen/) | Swashbuckle.AspNetCore.SwaggerGen | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [Swashbuckle.AspNetCore.SwaggerUI](https://www.nuget.org/packages/Swashbuckle.AspNetCore.SwaggerUI/) | Swashbuckle.AspNetCore.SwaggerUI | [MIT](https://github.com/domaindrivendev/Swashbuckle.AspNetCore/blob/master/LICENSE) | 
| [XObjects](https://www.nuget.org/packages/XObjects/) | European Commission (Eurostat) | [LGPL-3.0-only](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/licence.txt) |

