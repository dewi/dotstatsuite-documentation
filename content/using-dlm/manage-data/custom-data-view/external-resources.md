---
title: "Add links to external resources"
subtitle: 
comments: 
weight: 510

---

> Released in [XXXXX](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#XXXXXX)

to be complemented (once https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/161 is delivered)
