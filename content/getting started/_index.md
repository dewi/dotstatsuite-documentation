---
title: "Getting started"
subtitle: 
comments: false
weight: 20

---

### Our delivery and support streams' diagram

![DevOps diagram](/dotstatsuite-documentation/images/devops-schema.png)

In this diagram are represented the deliverables we provide (in green) based on the 3 different delivery approaches that we support.  

In blue are represented the areas where some technical support can be provided by third-parties' technical partner(s) with the following tasks:  
* Advanced support for source code installations for technical experts (in collaboration with the Developer advocate);
* Support for source code installations for technical newbies (creation of simple installation scripts, hand-holding);
* Advanced support for container based installations (using Docker images) on premise (onsite/own cloud) for technical experts;
* Software as a Service (SaaS): host pilot, test of production instances.
